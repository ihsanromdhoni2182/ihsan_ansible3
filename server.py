from flask import Flask, render_template, request
from app.config.middleware import checkLogin
from app.controllers import misc, user, automation, server, sshuser
import os

import logging
log = logging.getLogger('werkzeug')
# log.setLevel(logging.ERROR)

app = Flask(__name__)

## ---------- START USERS ---------- ##
@app.route("/users")
@checkLogin
def user_index():
    return user.index() 

@app.route("/users/create")
@checkLogin
def user_create():
    return user.create() 

@app.route("/users/store", methods=['POST'])
@checkLogin
def user_store():
    return user.store(request)

@app.route("/users/<int:id>/update", methods=['POST'])
@checkLogin
def user_update(id):
    return user.update(request, id)

@app.route("/users/<int:id>/edit")
@checkLogin
def user_edit(id):
    return user.edit(id)

@app.route("/users/<int:id>/delete")
@checkLogin
def user_delete(id):
    return user.delete(id)
## ---------- END USERS ---------- ##


## ---------- START SERVER ---------- ##
@app.route("/server")
@checkLogin
def server_index():
    return server.index()

@app.route("/server/create")
@checkLogin
def server_create():
    return server.create() 

@app.route("/server/store", methods=['POST'])
@checkLogin
def server_store():
    return server.store()

@app.route("/server/<int:id>/update", methods=['POST'])
@checkLogin
def server_update(id):
    return server.update(id)

@app.route("/server/<int:id>/edit")
@checkLogin
def server_edit(id):
    return server.edit(id)

@app.route("/server/<int:id>/delete")
@checkLogin
def server_delete(id):
    return server.delete(id)
## ---------- END SERVER ---------- ##


## ---------- START SSH USER ---------- ##
@app.route("/sshuser")
@checkLogin
def sshuser_index():
    return sshuser.index()

@app.route("/sshuser/create")
@checkLogin
def sshuser_create():
    return sshuser.create() 

@app.route("/sshuser/store", methods=['POST'])
@checkLogin
def sshuser_store():
    return sshuser.store()

@app.route("/sshuser/<int:id>/update", methods=['POST'])
@checkLogin
def sshuser_update(id):
    return sshuser.update(id)

@app.route("/sshuser/<int:id>/edit")
@checkLogin
def sshuser_edit(id):
    return sshuser.edit(id)

@app.route("/sshuser/<int:id>/delete")
@checkLogin
def sshuser_delete(id):
    return sshuser.delete(id)

@app.route("/sshuser/multiassign", methods=['POST'])
@checkLogin
def sshuser_multiassign():
    return automation.multiassign()

@app.route("/sshuser/multiremove", methods=['POST'])
@checkLogin
def sshuser_multiremove():
    return automation.multiremove()
## ---------- END SSH USER ---------- ##

## ---------- START SSH USER SERVER ---------- ##
@app.route("/sshuser/<int:id>/sshuser-server")
@checkLogin
def sshuser_server(id):
    return sshuser.sshuser_server(id)
## ---------- END SSH USER SERVER ---------- ##

## ---------- START USER SERVER CREDENTIAL ---------- ##
@app.route("/user-server-credential")
@checkLogin
def user_server_credential():
    return server.user_server_credential()

@app.route("/user-server-credential/create")
@checkLogin
def user_server_credential_create():
    return server.user_server_credential_create() 

@app.route("/user-server-credential/store", methods=['POST'])
@checkLogin
def user_server_credential_store():
    return server.user_server_credential_store()

@app.route("/user-server-credential/<int:id>/update", methods=['POST'])
@checkLogin
def user_server_credential_update(id):
    return server.user_server_credential_update(id)

@app.route("/user-server-credential/<int:id>/edit")
@checkLogin
def user_server_credential_edit(id):
    return server.user_server_credential_edit(id)

@app.route("/user-server-credential/<int:id>/delete")
@checkLogin
def user_server_credential_delete(id):
    return server.user_server_credential_delete(id)
## ---------- END USER SERVER CREDENTIAL ---------- ##

## ---------- START AUTOMATION ---------- ##
@app.route("/automation/sync-key-server/<int:id>")
@checkLogin
def sync_key_server(id):
    return automation.sync_key_server(id)

@app.route('/automation/change-port/<int:id>', methods=['POST'])
@checkLogin
def change_port_ssh(id):
    return automation.change_port_ssh(id)


@app.route("/sshuser/<int:sshuser_id>/<int:server_id>/add-to-server")
@checkLogin
def create_ssh_user(sshuser_id, server_id):
    return automation.create_ssh_user(sshuser_id, server_id)


@app.route("/sshuser/<int:sshuser_id>/<int:server_id>/remove-from-server")
@checkLogin
def remove_ssh_user(sshuser_id, server_id):
    return automation.remove_ssh_user(sshuser_id, server_id)

# test ping server
@app.route("/automation/test-connection/<int:server_id>")
@checkLogin
def test_server_connection(server_id):
    return automation.test_server_connection(server_id)
## ---------- EBD AUTOMATION ---------- ##

##MISC
@app.route("/")
@checkLogin
def index():
    return misc.index()

##MISC
@app.route("/login")
def login():
    return misc.login()

@app.route("/doLogin", methods=['POST'])
def doLogin():
    return misc.doLogin(request.form)

@app.route("/logout")
def logout():
    return misc.logout()

app.secret_key = '3RDLwwtFttGSxkaDHyFTmvGytBJ2MxWT8ynWm2y79G8jm9ugYxFFDPdHcBBnHp6E'
app.config['SESSION_TYPE'] = 'filesystem'

@app.context_processor
def inject_stage_and_region():
    return dict(APP_NAME=os.environ.get("APP_NAME"),
        APP_AUTHOR=os.environ.get("APP_AUTHOR"),
        APP_TITLE=os.environ.get("APP_TITLE"),
        APP_LOGO=os.environ.get("APP_LOGO"),
        history_all=misc.data_history())

# def data_history():
#     def history_all():
#         return misc.data_history()
#     return dict(history_all=history_all)

if __name__ == "__main__":
    app.run()
    #app.run(host='0.0.0.0', port=5299)