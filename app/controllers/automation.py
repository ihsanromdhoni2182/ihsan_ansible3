from flask import render_template, redirect, session, url_for, flash, request
from app.models.Server import *
from app.models.Sshuser import *
from app.models.SshuserServer import *
from app.models.HistoryAction import *


import subprocess
import yaml
import os
import sys

def test_server_connection(server_id):
    server = Server.join('server_user_credential AS suc', 'suc.id', '=', 'servers.user_credential_id')\
        .select('servers.*', 'suc.username', 'suc.password', 'suc.private_key', 'suc.public_key').find(server_id).serialize()

    create_inventory(server)
    yaml_path = create_connection_test()

    try:
        run_ansible = subprocess.check_output(['sh', './static/bash_file/run_playbook.sh', yaml_path])
    except subprocess.CalledProcessError as e:
        run_ansible = e.output
        print(("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output)))

    print('#########################')
    arr_result  = [i for i in run_ansible.decode("utf-8").split('\n') if i]
    last_result = [i for i in arr_result[-1].split(' ') if i]

    failed = 0
    for l in last_result:
        rp = l.split('=')
        if len(rp)>1:
            if rp[0] == 'failed':
                failed+=int(rp[1])
            elif rp[0] == 'unreachable':
                failed+=int(rp[1])
    # print(failed)

    save_action('Test Connection', server_id, "<br />".join(run_ansible.decode("utf-8").split("\n")))
    if failed == 0:
        server = Server.find(server_id)
        server.connection_test = 'Success'
        server.save()
    else:
        server = Server.find(server_id)
        server.connection_test = 'Failed'
        server.save()
    # print("<br />".join(run_ansible.decode("utf-8").split("\n")))
    return redirect(url_for('server_index'))
    

def sync_key_server(server_id):
    # create_key = subprocess.call(['sh', './static/bash_file/create_ssh_keygen.sh'])
    # Get server
    # print('========================')
    # print(create_key)

    # server = Server.find(server_id).serialize()
    server = Server.join('server_user_credential AS suc', 'suc.id', '=', 'servers.user_credential_id')\
        .select('servers.*', 'suc.username', 'suc.password', 'suc.private_key', 'suc.public_key').find(server_id).serialize()
    print('========================')
    print(server)

    create_inventory(server)
    create_store_root_ssh_key(server['username'], server['public_key'])

    # Run ansible bash
    # ansible_send_key = subprocess.call(['sh', './static/bash_file/store_this_vm_key.sh'])

    try:
        ansible_send_key = subprocess.check_output(['sh', './static/bash_file/store_this_vm_key.sh'])
    except subprocess.CalledProcessError as e:
        ansible_send_key = e.output
        print(("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output)))
        # raise RuntimeError("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output))

    print('#########################')
    arr_result  = [i for i in ansible_send_key.decode("utf-8").split('\n') if i]
    last_result = [i for i in arr_result[-1].split(' ') if i]

    failed = 0
    for l in last_result:
        rp = l.split('=')
        if len(rp)>1:
            if rp[0] == 'failed':
                failed+=int(rp[1])
            elif rp[0] == 'unreachable':
                failed+=int(rp[1])

    save_action('Sync Key Server', server_id, "---")
    if failed == 0:
        # Change status sync to 1
        sdata = Server.find(server_id)
        sdata.status = 1
        sdata.save()

        flash('Sync RSA key server success<br/>'+"<br />".join(ansible_send_key.decode("utf-8").split("\n")), 'success')
        return redirect(url_for('server_index'))
    else:
        flash('Sync RSA key Failed<br/>'+"<br />".join(ansible_send_key.decode("utf-8").split("\n")), 'danger')
        return redirect(url_for('server_index'))

def change_port_ssh(id):
    post   = request.form
    server = Server.join('server_user_credential AS suc', 'suc.id', '=', 'servers.user_credential_id')\
        .select('servers.*', 'suc.username', 'suc.password', 'suc.private_key', 'suc.public_key').find(id).serialize()
    # Set inventory to current server
    create_inventory(server)
    # Create playbook
    create_change_port_ssh(post['port'])

    # Run ansible bash
    try:
        run_ansible = subprocess.check_output(['sh', './static/bash_file/change_ssh_port.sh'])
    except subprocess.CalledProcessError as e:
        run_ansible = e.output
        print(("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output)))

    print('#########################')
    arr_result  = [i for i in run_ansible.decode("utf-8").split('\n') if i]
    last_result = [i for i in arr_result[-1].split(' ') if i]

    failed = 0
    for l in last_result:
        rp = l.split('=')
        if len(rp)>1:
            if rp[0] == 'failed':
                failed+=int(rp[1])
            elif rp[0] == 'unreachable':
                failed+=int(rp[1])


    save_action('Change Port SSH', id, "<br />".join(run_ansible.decode("utf-8").split("\n")))
    if failed == 0:
        # Change status sync to 1
        sdata = Server.find(id)
        sdata.port = post['port']
        sdata.save()

        flash('Change Port server success<br/>'+"<br />".join(run_ansible.decode("utf-8").split("\n")), 'success')
        return redirect(url_for('server_index'))
    else:
        flash('Change Port Failed<br/>'+"<br />".join(run_ansible.decode("utf-8").split("\n")), 'danger')
        return redirect(url_for('server_index'))
    return "OK"

def create_ssh_user(sshuser_id, server_id):
    # Get server detail
    server = Server.join('server_user_credential AS suc', 'suc.id', '=', 'servers.user_credential_id')\
        .select('servers.*', 'suc.username', 'suc.password', 'suc.private_key', 'suc.public_key').find(server_id).serialize()
    # Get Ssh user detail
    ssh_user     = Sshuser.find(sshuser_id).serialize()
    pub_key_path = os.path.abspath('static/pub_key/'+ssh_user['pub_key'])

    # Set inventory to current server
    create_inventory(server)
    yaml_path = create_new_user(ssh_user['username'], pub_key_path)

    try:
        run_ansible = subprocess.check_output(['sh', './static/bash_file/run_playbook.sh', yaml_path])
    except subprocess.CalledProcessError as e:
        run_ansible = e.output
        print(("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output)))

    print('#########################')
    arr_result  = [i for i in run_ansible.decode("utf-8").split('\n') if i]
    last_result = [i for i in arr_result[-1].split(' ') if i]

    failed = 0
    for l in last_result:
        rp = l.split('=')
        if len(rp)>1:
            if rp[0] == 'failed':
                failed+=int(rp[1])
            elif rp[0] == 'unreachable':
                failed+=int(rp[1])

    save_action('Add user to Server', server_id, "<br />".join(run_ansible.decode("utf-8").split("\n")))
    if failed == 0:
        # Change status sync to 1

        sshuser_server = SshuserServer.where('sshuser_id', sshuser_id).where('server_id', server_id).first()
        if sshuser_server is None:
            sshuser_server = SshuserServer()

        sshuser_server.server_id  = server_id
        sshuser_server.sshuser_id = sshuser_id
        sshuser_server.is_sync    = 1

        sshuser_server.save()

        flash('User added to server Success<br/>'+"<br />".join(run_ansible.decode("utf-8").split("\n")), 'success')
        return redirect(url_for('sshuser_server', id=sshuser_id))
    else:
        flash('User added to server Failed<br/>'+"<br />".join(run_ansible.decode("utf-8").split("\n")), 'danger')
        return redirect(url_for('sshuser_server', id=sshuser_id))

def multiassign():
    form = request.form

    server = form.getlist('server')
    if len(server) == 0:
        flash('Server tidak di pilih', 'danger')
        return redirect(url_for("sshuser_index"))

    user = form.getlist('user')
    if len(user) == 0:
        flash('User tidak di pilih', 'danger')
        return redirect(url_for("sshuser_index"))
    
    total_failed = 0

    flash_success = ""
    flash_failed  = ""
    for s in server:
        server_id = s
        for u in user:
            sshuser_id = u

            # Get server detail
            server = Server.join('server_user_credential AS suc', 'suc.id', '=', 'servers.user_credential_id')\
                .select('servers.*', 'suc.username', 'suc.password', 'suc.private_key', 'suc.public_key').find(server_id).serialize()
            # Get Ssh user detail
            ssh_user     = Sshuser.find(sshuser_id).serialize()
            pub_key_path = os.path.abspath('static/pub_key/'+ssh_user['pub_key'])

            # Set inventory to current server
            create_inventory(server)
            yaml_path = create_new_user(ssh_user['username'], pub_key_path)

            try:
                run_ansible = subprocess.check_output(['sh', './static/bash_file/run_playbook.sh', yaml_path])
            except subprocess.CalledProcessError as e:
                run_ansible = e.output
                print(("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output)))

            print('#########################')
            arr_result  = [i for i in run_ansible.decode("utf-8").split('\n') if i]
            last_result = [i for i in arr_result[-1].split(' ') if i]

            failed = 0
            for l in last_result:
                rp = l.split('=')
                if len(rp)>1:
                    if rp[0] == 'failed':
                        failed+=int(rp[1])
                    elif rp[0] == 'unreachable':
                        failed+=int(rp[1])
            if failed == 0:
                # Change status sync to 1

                sshuser_server = SshuserServer.where('sshuser_id', sshuser_id).where('server_id', server_id).first()
                if sshuser_server is None:
                    sshuser_server = SshuserServer()

                sshuser_server.server_id  = server_id
                sshuser_server.sshuser_id = sshuser_id
                sshuser_server.is_sync    = 1

                sshuser_server.save()
                flash_success += 'User added to server Success<br/>'+"<br />".join(run_ansible.decode("utf-8").split("\n"))
            else:
                total_failed += 1
                flash_failed += 'User added to server Failed<br/>'+"<br />".join(run_ansible.decode("utf-8").split("\n"))

    flash(flash_success, 'success')
    if total_failed > 0:
        flash(flash_failed, 'danger')

    return redirect(url_for("sshuser_index"))

def remove_ssh_user(sshuser_id, server_id):
    # Get server detail
    server = Server.join('server_user_credential AS suc', 'suc.id', '=', 'servers.user_credential_id')\
        .select('servers.*', 'suc.username', 'suc.password', 'suc.private_key', 'suc.public_key').find(server_id).serialize()
    # Get Ssh user detail
    ssh_user     = Sshuser.find(sshuser_id).serialize()

    # Set inventory to current server
    create_inventory(server)
    yaml_path = create_remove_user(ssh_user['username'])

    try:
        run_ansible = subprocess.check_output(['sh', './static/bash_file/run_playbook.sh', yaml_path])
    except subprocess.CalledProcessError as e:
        run_ansible = e.output
        print(("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output)))

    print('#########################')
    arr_result  = [i for i in run_ansible.decode("utf-8").split('\n') if i]
    last_result = [i for i in arr_result[-1].split(' ') if i]

    failed = 0
    for l in last_result:
        rp = l.split('=')
        if len(rp)>1:
            if rp[0] == 'failed':
                failed+=int(rp[1])
            elif rp[0] == 'unreachable':
                failed+=int(rp[1])

    save_action('Remove User From Server', server_id, "<br />".join(run_ansible.decode("utf-8").split("\n")))
    if failed == 0:
        # Change status sync to 1

        sshuser_server = SshuserServer.where('sshuser_id', sshuser_id).where('server_id', server_id).first()
        if sshuser_server is None:
            sshuser_server = SshuserServer()

        sshuser_server.is_sync = 0

        sshuser_server.save()

        flash('User remove from server Success<br/>'+"<br />".join(run_ansible.decode("utf-8").split("\n")), 'success')
        return redirect(url_for('sshuser_server', id=sshuser_id))
    else:
        flash('User remove from server Failed<br/>'+"<br />".join(run_ansible.decode("utf-8").split("\n")), 'danger')
        return redirect(url_for('sshuser_server', id=sshuser_id))
    return "OK"


def multiremove():

    form = request.form

    server = form.getlist('server')
    if len(server) == 0:
        flash('Server tidak di pilih', 'danger')
        return redirect(url_for("sshuser_index"))

    user = form.getlist('user')
    if len(user) == 0:
        flash('User tidak di pilih', 'danger')
        return redirect(url_for("sshuser_index"))

    total_failed = 0

    flash_success = ""
    flash_failed  = ""

    for s in server:
        server_id = s
        for u in user:
            sshuser_id = u
            # Get server detail
            server = Server.join('server_user_credential AS suc', 'suc.id', '=', 'servers.user_credential_id')\
                .select('servers.*', 'suc.username', 'suc.password', 'suc.private_key', 'suc.public_key').find(server_id).serialize()
            # Get Ssh user detail
            ssh_user     = Sshuser.find(sshuser_id).serialize()

            # Set inventory to current server
            create_inventory(server)
            yaml_path = create_remove_user(ssh_user['username'])

            try:
                run_ansible = subprocess.check_output(['sh', './static/bash_file/run_playbook.sh', yaml_path])
            except subprocess.CalledProcessError as e:
                run_ansible = e.output
                print(("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output)))

            print('#########################')
            arr_result  = [i for i in run_ansible.decode("utf-8").split('\n') if i]
            last_result = [i for i in arr_result[-1].split(' ') if i]

            failed = 0
            for l in last_result:
                rp = l.split('=')
                if len(rp)>1:
                    if rp[0] == 'failed':
                        failed+=int(rp[1])
                    elif rp[0] == 'unreachable':
                        failed+=int(rp[1])

            save_action('Remove User From Server', server_id, "<br />".join(run_ansible.decode("utf-8").split("\n")))
            if failed == 0:
                # Change status sync to 1

                sshuser_server = SshuserServer.where('sshuser_id', sshuser_id).where('server_id', server_id).first()
                if sshuser_server is None:
                    sshuser_server = SshuserServer()

                sshuser_server.is_sync = 0

                sshuser_server.save()

                flash_success += 'User remove from server Success<br/>'+"<br />".join(run_ansible.decode("utf-8").split("\n"))
            else:
                flash_failed += 'User remove from server Failed<br/>'+"<br />".join(run_ansible.decode("utf-8").split("\n"))

    flash(flash_success, 'success')
    if total_failed > 0:
        flash(flash_failed, 'danger')

    return redirect(url_for("sshuser_index"))

# ===== CREATE ANSIBLE FUNCTION =====
def create_inventory(server_data):
    key_path = os.path.abspath('static/user_key/'+server_data['private_key'])
    data = {
        'all' : {
            'hosts':{
                server_data['server']: {
                    'ansible_user'     : server_data['username'],
                    'ansible_ssh_pass' : server_data['password'],
                    'ansible_port'     : server_data['port'],
                    'ansible_ssh_private_key_file' : key_path
                }
            }
        }
    }

    f = open('static/server_inventory.yml', 'w+')
    yaml.dump(data, f, allow_unicode=True)
    f.close()
    # print(data)
    return True

def create_store_root_ssh_key(username, pub_key):
    user_login = os.getlogin()
    # ssh_key_path = '/'+user_login+'/.ssh/id_rsa.pub'
    # ssh_key_path = '~/.ssh/id_rsa.pub'
    ssh_key_path = os.path.abspath('static/user_key/'+pub_key)
    print('=======================')
    print(ssh_key_path)

    data = [{
            'name'  : 'FirstPlayBook',
            'hosts' : 'all',
            'tasks' : [{
                'name': 'Install Key',
                'authorized_key':{
                    'user': username,
                    'state': 'present',
                    'key': '{{lookup(\"file\", \"'+ssh_key_path+'\")}}'
                }
            },
            {
                'name' : 'Disable Password Login',
                'lineinfile':{
                    'dest'   : '/etc/ssh/sshd_config',
                    'regexp' : '^PasswordAuthentication',
                    'line'   : "PasswordAuthentication no",
                    'state'  : 'present',
                    'backup' : 'yes'
                },
                'notify' : ['restart ssh']
            }],
            'handlers' : [{
                'name'   : 'restart ssh',
                'service': {
                    'name' : 'sshd',
                    'state': 'restarted'
                }
            }]
        }]

    f = open('static/server_first.yml', 'w+')
    yaml.dump(data, f, allow_unicode=True)
    f.close()
    return True

def create_change_port_ssh(port):
    data = [{
            'name'  : 'ChangePortPlayBook',
            'hosts' : 'all',
            'tasks' : [
                {
                    'name' : 'Chnage SSH PORT',
                    'lineinfile':{
                        'dest'   : '/etc/ssh/sshd_config',
                        'regexp' : '^Port',
                        'line'   : "Port "+str(port),
                        'state'  : 'present',
                        'backup' : 'yes'
                    },
                    'notify' : ['restart ssh']
                }
            ],
            'handlers' : [{
                'name'   : 'restart ssh',
                'service': {
                    'name' : 'sshd',
                    'state': 'restarted'
                }
            }]
        }]
    f = open('static/change_port_ssh.yml', 'w+')
    yaml.dump(data, f, allow_unicode=True)
    f.close()
    return True

def create_new_user(username, pub_key_path):
    data = [
        {
            'name'         : 'CreateNewUserPlaybook',
            'hosts'        : 'all',
            'become'       : 'true',
            'gather_facts' : 'false',
            'vars'         : {
                'username' : username,
                'userpass' : username
            },
            'tasks'  : [
                {
                    'name' : 'Create User',
                    'user' : {
                        'name'              : '{{ username }}',
                        'state'             : 'present' ,
                        'shell'             : '/bin/bash',
                        'password'          : '{{ userpass | password_hash("sha512") }}',
                        'update_password'   : 'on_create',
                        'groups'            : 'sudo',
                        'append'            : 'yes'
                    }
                },
                {
                    'name' : 'Deploy SSH Public Key',
                    'authorized_key':{
                        'user'  : '{{ username }}',
                        'state' : 'present',
                        'key'   : '{{lookup(\"file\", \"'+pub_key_path+'\")}}'

                    }
                }

            ]
        }
    ]
    yaml_path = 'static/create_user_ssh.yml'
    f         = open(yaml_path, 'w+')
    yaml.dump(data, f, allow_unicode=True)
    f.close()
    return yaml_path

def create_remove_user(username):
    data = [
        {
            'name'         : 'RemoveUserPlaybook',
            'hosts'        : 'all',
            'become'       : 'true',
            'gather_facts' : 'false',
            'vars'         : {
                'username' : username
            },
            'tasks'  : [
                {
                    'name' : 'Remove User',
                    'user' : {
                        'name'   : '{{ username }}',
                        'state'  : 'absent' ,
                        'remove' : 'true'
                    }
                }
            ]
        }
    ]
    yaml_path = 'static/remove_ssh_user.yml'
    f         = open(yaml_path, 'w+')
    yaml.dump(data, f, allow_unicode=True)
    f.close()
    return yaml_path

def create_connection_test():
    data = [{
        'name' : 'TestConnection',
        'hosts' : 'all',
        'tasks' : [{
            'name' : 'Test Connection',
            'ping' : ''
        }]
    }]

    yaml_path = 'static/test_server_connection.yml'
    f         = open(yaml_path, 'w+')
    yaml.dump(data, f, allow_unicode=True)
    f.close()
    return yaml_path




# ================= SAVE ACTION =================
def save_action(action_name, server_id, ansible_message):
    history = HistoryAction()
    history.action          = action_name
    history.server_id       = server_id
    history.ansible_message = ansible_message
    history.save()
    return True
