from flask import render_template, redirect, session, url_for, flash, request
from werkzeug.utils import secure_filename

from app.models.Server import *
from app.models.Sshuser import *
from app.models.SshuserServer import *

import os

def index():
	try:
		data   = Sshuser.get().serialize()
		server = Server.where('status', 1).get().serialize()
		return render_template('pages/sshuser/index.html', data=data, server=server)
	except Exception as e:
		return 'Something went wrong ' + str(e)


def create():
	return render_template('pages/sshuser/create.html')


def store():
	try:
		post          = request.form
		uploaded_file = request.files['pubkey']
		username      = post['username']

		# Check not allowed username
		not_allowed_username = ['admin', 'root']
		if username in not_allowed_username:
			flash('Username not allowed', 'danger')
			return redirect(url_for('sshuser_index'))

		allowed_extention = ['.pub']

		filename = secure_filename(uploaded_file.filename)
		file_ext = os.path.splitext(filename)[1]

		# Check allowed extention
		if file_ext not in allowed_extention:
			flash('Extention not allowed', 'danger')
			return redirect(url_for('sshuser_index'))

		# Check if username already exists
		check_username = Sshuser.where('username', username).first()
		if check_username is not None:
			flash('Username already exist', 'danger')
			return redirect(url_for('sshuser_index'))


		file_path = "static/pub_key/"+username+ file_ext
		uploaded_file.save(file_path)

		data = Sshuser()
		data.username = username
		data.pub_key  = username+file_ext
		data.save()
		# data.server   = post['server']
		# data.username = post['username']
		# data.port     = post['port']
		# data.password = post['password']
		# data.save()

		flash('Data berhasil di simpan.!', 'success')
		return redirect(url_for('sshuser_index'))
	except Exception as e:
		return 'Something went wrong ' + str(e)

def edit(id):
	try:
		data = Sshuser.find(id)
		return render_template('pages/sshuser/edit.html', data=data)
	except Exception as e:
		return 'Something went wrong ' + str(e)

def update(id):
	try:
		post          = request.form
		uploaded_file = request.files['pubkey']
		username      = post['username']

		data = Sshuser.find(id)
		# data.server   = post['server']
		# data.username = post['username']
		# data.password = post['password']
		# data.save()

		filename = secure_filename(uploaded_file.filename)
		if len(filename) > 0:
			file_ext = os.path.splitext(filename)[1]

			allowed_extention = ['.pub']

			# Check allowed extention
			if file_ext not in allowed_extention:
				flash('Extention not allowed', 'danger')
				return redirect(url_for('sshuser_index'))
			file_path = "static/pub_key/"+username+ file_ext
			if os.path.exists(file_path):
				os.remove(file_path)
			uploaded_file.save(file_path)

		# print(len(filename))
		# return "Ok"

		flash('Data berhasil di update.!', 'success')
		return redirect(url_for('sshuser_index'))
	except Exception as e:
		return 'Something went wrong ' + str(e)


def delete(id):
	try:
		delete = Sshuser.find(id).delete()
		flash('Data berhasil di update.!', 'success')
		return redirect(url_for("server_index"))
	except Exception as e:
		return 'Something went wrong ' + str(e)


def sshuser_server(sshuser_id):
	try:
		data = Sshuser.find(sshuser_id)

		server = Server.where('status', 1).get().serialize()
		for s in range(len(server)):
			# Get assigned server
			server_id = server[s]['id']
			sshuser_server = SshuserServer.where('server_id', server_id).where('sshuser_id', sshuser_id).first()
			if sshuser_server is None:
				sync_status = 0
			else:
				sshuser_server = sshuser_server.serialize()
				sync_status = sshuser_server['is_sync']

			server[s]['sync_status'] = sync_status
		# print(server)

		return render_template('pages/sshuser/index_server.html', data=data, server=server,
			sshuser_id=sshuser_id)
	except Exception as e:
		return 'Something went wrong ' + str(e)

