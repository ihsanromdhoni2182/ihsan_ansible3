from flask import render_template, redirect, session, url_for, flash, request
from werkzeug.utils import secure_filename
from app.models.Server import *
from app.models.ServerUserCredential import *

import datetime
import os

def index():
	try:
		data = Server.join('server_user_credential AS suc', 'suc.id', '=', 'servers.user_credential_id').get().serialize()
		return render_template('pages/server/index.html', data=data)
	except Exception as e:
		return 'Something went wrong ' + str(e)


def create():
	cred_user = ServerUserCredential.get().serialize()
	return render_template('pages/server/create.html', cred_user=cred_user)


def store():
	try:
		post = request.form

		data = Server()
		data.server             = post['server']
		data.port               = post['port']
		data.user_credential_id = post['user_cred']
		data.save()

		flash('Data berhasil di simpan.!', 'success')
		# return redirect(url_for('server_index'))
		return redirect(url_for('test_server_connection', server_id=data.id))
		
	except Exception as e:
		return 'Something went wrong ' + str(e)


def edit(id):
	try:
		data      = Server.find(id)
		cred_user = ServerUserCredential.get().serialize()
		return render_template('pages/server/edit.html', data=data, cred_user=cred_user)
	except Exception as e:
		return 'Something went wrong ' + str(e)

def update(id):
	try:
		post = request.form

		data = Server.find(id)
		data.server   = post['server']
		data.port     = post['port']
		data.user_credential_id = post['user_cred']
		data.save()

		flash('Data berhasil di update.!', 'success')
		# return redirect(url_for('server_index'))
		return redirect(url_for('test_server_connection', server_id=data.id))
	except Exception as e:
		return 'Something went wrong ' + str(e)


def delete(id):
	try:
		delete = Server.find(id).delete()
		flash('Data berhasil di update.!', 'success')
		return redirect(url_for("server_index"))
	except Exception as e:
		return 'Something went wrong ' + str(e)
	
# === USER SERVER CREDENTIAL ===
def user_server_credential():
	data = ServerUserCredential.get().serialize()
	return render_template('pages/server/user_server.html', data=data)

def user_server_credential_create():
	return render_template('pages/server/user_server_create.html')

def user_server_credential_store():
	post = request.form
	dnow = datetime.datetime.now()
	
	uploaded_file_priv = request.files['private']
	uploaded_file_pub  = request.files['public']

	# **
	# Proses Private
	# **
	filename_priv  = secure_filename(uploaded_file_priv.filename)
	file_ext_priv  = os.path.splitext(filename_priv)[1]

	file_name_priv = dnow.strftime("%Y%m%d%H%M%S")+"_private"+file_ext_priv
	file_path_priv = "static/user_key/"+ file_name_priv
	uploaded_file_priv.save(file_path_priv)
	os.chmod(file_path_priv, 400)

	# **
	# Proses Public
	# **
	filename_pub   = secure_filename(uploaded_file_pub.filename)
	file_ext_pub   = os.path.splitext(filename_pub)[1]

	file_name_pub = dnow.strftime("%Y%m%d%H%M%S")+"_public"+file_ext_pub
	file_path_pub = "static/user_key/"+ file_name_pub
	uploaded_file_pub.save(file_path_pub)

	# Add new user credential
	user = ServerUserCredential()
	user.name        = post['name']
	user.username    = post['username']
	user.password    = post['password']
	user.private_key = file_name_priv
	user.public_key  = file_name_pub
	user.save()

	flash('User Credential berhasil di simpan.!', 'success')	
	return redirect(url_for('user_server_credential'))

def user_server_credential_edit(id):
	data = ServerUserCredential.find(id)
	return render_template('pages/server/user_server_edit.html', data=data)


def user_server_credential_update(id):
	post = request.form
	dnow = datetime.datetime.now()

	uploaded_file_priv = request.files['private']
	uploaded_file_pub  = request.files['public']

	filename_priv  = secure_filename(uploaded_file_priv.filename)
	filename_pub   = secure_filename(uploaded_file_pub.filename)

	print(len(filename_priv))
	print(len(filename_pub))

	user = ServerUserCredential.find(id)
	user.name        = post['name']
	user.username    = post['username']
	user.password    = post['password']

	if len(filename_priv) > 0:
		# **
		# Proses Private
		# **
		filename_priv  = secure_filename(uploaded_file_priv.filename)
		file_ext_priv  = os.path.splitext(filename_priv)[1]

		file_name_priv = dnow.strftime("%Y%m%d%H%M%S")+"_private"+file_ext_priv
		file_path_priv = "static/user_key/"+ file_name_priv
		uploaded_file_priv.save(file_path_priv)

		user.private_key = file_name_priv

	if len(filename_pub) > 0:
		# **
		# Proses Public
		# **
		filename_pub   = secure_filename(uploaded_file_pub.filename)
		file_ext_pub   = os.path.splitext(filename_pub)[1]

		file_name_pub = dnow.strftime("%Y%m%d%H%M%S")+"_public"+file_ext_pub
		file_path_pub = "static/user_key/"+ file_name_pub
		uploaded_file_pub.save(file_path_pub)

		user.public_key  = file_name_pub

	user.save()
	flash('User Credential berhasil di update.!', 'success')	
	return redirect(url_for('user_server_credential'))

def user_server_credential_delete(id):
	data = ServerUserCredential.find(id).delete()
	flash('User Credential berhasil di hapus.!', 'success')	
	return redirect(url_for('user_server_credential'))