from flask import render_template, redirect, session, url_for, flash, request
from werkzeug.utils import secure_filename
from app.models.User import *
from app.models.HistoryAction import *
import bcrypt
import datetime
import os

def index():
	return render_template('pages/dashboard.html')

##AUTH
def login():
	if "user" in session:
		return redirect(url_for("index"))
	else:
		return render_template('pages/login.html')

def doLogin(data):
	try:
		user = User.get_by_username(data['username'])

		if user == None:
			flash('Username tidak terdaftar.!', 'danger')
			return redirect(url_for('login'))
		if bcrypt.checkpw(data['password'].encode('utf8'), user['password'].encode('utf8')):
			session['user'] = user
			return redirect(url_for("index"))
		else:
			flash('Password yang dimasukan salah.!', 'danger')
			return redirect(url_for('login'))
	except Exception as e:
		raise e

def logout():
	if "user" in session:
		session.pop("user", None)

	return redirect(url_for("login"))

def data_history():
	data = HistoryAction.join('servers', 'servers.id','=','history_action.server_id').order_by('history_action.id', 'DESC').get().serialize()
	print('==========================')
	print(data)
	return data