/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : localhost:3306
 Source Schema         : aplikasi_ihsan_ansible

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 27/12/2022 17:55:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for history_action
-- ----------------------------
DROP TABLE IF EXISTS `history_action`;
CREATE TABLE `history_action`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `server_id` int(11) NULL DEFAULT NULL,
  `ansible_message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of history_action
-- ----------------------------
INSERT INTO `history_action` VALUES (1, 'Test Connection', 3, '<br />PLAY [TestConnection] **********************************************************<br /><br />TASK [Gathering Facts] *********************************************************<br />fatal: [103.167.34.139]: UNREACHABLE! => {\"changed\": false, \"msg\": \"Invalid/incorrect password: Permission denied, please try again.\", \"unreachable\": true}<br /><br />PLAY RECAP *********************************************************************<br />103.167.34.139             : ok=0    changed=0    unreachable=1    failed=0    skipped=0    rescued=0    ignored=0   <br /><br />', '2022-12-26 15:59:35', '2022-12-26 15:59:35', NULL);
INSERT INTO `history_action` VALUES (2, 'Test Connection', 3, '<br />PLAY [TestConnection] **********************************************************<br /><br />TASK [Gathering Facts] *********************************************************<br />fatal: [103.167.34.139]: UNREACHABLE! => {\"changed\": false, \"msg\": \"Invalid/incorrect password: Permission denied, please try again.\", \"unreachable\": true}<br /><br />PLAY RECAP *********************************************************************<br />103.167.34.139             : ok=0    changed=0    unreachable=1    failed=0    skipped=0    rescued=0    ignored=0   <br /><br />', '2022-12-26 16:06:06', '2022-12-26 16:06:06', NULL);
INSERT INTO `history_action` VALUES (3, 'Test Connection', 2, '<br />PLAY [TestConnection] **********************************************************<br /><br />TASK [Gathering Facts] *********************************************************<br />ok: [103.167.35.72]<br /><br />TASK [Test Connection] *********************************************************<br />ok: [103.167.35.72]<br /><br />PLAY RECAP *********************************************************************<br />103.167.35.72              : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   <br /><br />', '2022-12-26 16:06:30', '2022-12-26 16:06:30', NULL);
INSERT INTO `history_action` VALUES (4, 'Test Connection', 1, '<br />PLAY [TestConnection] **********************************************************<br /><br />TASK [Gathering Facts] *********************************************************<br />ok: [103.167.35.71]<br /><br />TASK [Test Connection] *********************************************************<br />ok: [103.167.35.71]<br /><br />PLAY RECAP *********************************************************************<br />103.167.35.71              : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   <br /><br />', '2022-12-26 16:06:44', '2022-12-26 16:06:44', NULL);
INSERT INTO `history_action` VALUES (5, 'Test Connection', 3, '<br />PLAY [TestConnection] **********************************************************<br /><br />TASK [Gathering Facts] *********************************************************<br />ok: [103.167.34.139]<br /><br />TASK [Test Connection] *********************************************************<br />ok: [103.167.34.139]<br /><br />PLAY RECAP *********************************************************************<br />103.167.34.139             : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   <br /><br />', '2022-12-26 16:13:19', '2022-12-26 16:13:19', NULL);
INSERT INTO `history_action` VALUES (6, 'Test Connection', 3, '<br />PLAY [TestConnection] **********************************************************<br /><br />TASK [Gathering Facts] *********************************************************<br />ok: [103.167.34.139]<br /><br />TASK [Test Connection] *********************************************************<br />ok: [103.167.34.139]<br /><br />PLAY RECAP *********************************************************************<br />103.167.34.139             : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   <br /><br />', '2022-12-26 16:13:32', '2022-12-26 16:13:32', NULL);
INSERT INTO `history_action` VALUES (7, 'Test Connection', 3, '<br />PLAY [TestConnection] **********************************************************<br /><br />TASK [Gathering Facts] *********************************************************<br />ok: [103.167.34.139]<br /><br />TASK [Test Connection] *********************************************************<br />ok: [103.167.34.139]<br /><br />PLAY RECAP *********************************************************************<br />103.167.34.139             : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   <br /><br />', '2022-12-26 16:13:44', '2022-12-26 16:13:44', NULL);
INSERT INTO `history_action` VALUES (8, 'Test Connection', 3, '<br />PLAY [TestConnection] **********************************************************<br /><br />TASK [Gathering Facts] *********************************************************<br />fatal: [103.167.34.139]: UNREACHABLE! => {\"changed\": false, \"msg\": \"Failed to connect to the host via ssh: ssh: connect to host 103.167.34.139 port 32: Connection refused\", \"unreachable\": true}<br /><br />PLAY RECAP *********************************************************************<br />103.167.34.139             : ok=0    changed=0    unreachable=1    failed=0    skipped=0    rescued=0    ignored=0   <br /><br />', '2022-12-26 16:13:56', '2022-12-26 16:13:56', NULL);
INSERT INTO `history_action` VALUES (9, 'Test Connection', 3, '<br />PLAY [TestConnection] **********************************************************<br /><br />TASK [Gathering Facts] *********************************************************<br />fatal: [103.167.34.139]: UNREACHABLE! => {\"changed\": false, \"msg\": \"Failed to connect to the host via ssh: ssh: connect to host 103.167.34.139 port 32: Connection refused\", \"unreachable\": true}<br /><br />PLAY RECAP *********************************************************************<br />103.167.34.139             : ok=0    changed=0    unreachable=1    failed=0    skipped=0    rescued=0    ignored=0   <br /><br />', '2022-12-26 16:14:04', '2022-12-26 16:14:04', NULL);
INSERT INTO `history_action` VALUES (10, 'Test Connection', 4, '<br />PLAY [TestConnection] **********************************************************<br /><br />TASK [Gathering Facts] *********************************************************<br />ok: [103.167.34.139]<br /><br />TASK [Test Connection] *********************************************************<br />ok: [103.167.34.139]<br /><br />PLAY RECAP *********************************************************************<br />103.167.34.139             : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   <br /><br />', '2022-12-27 17:20:01', '2022-12-27 17:20:01', NULL);
INSERT INTO `history_action` VALUES (11, 'Test Connection', 4, '<br />PLAY [TestConnection] **********************************************************<br /><br />TASK [Gathering Facts] *********************************************************<br />fatal: [103.167.34.139]: UNREACHABLE! => {\"changed\": false, \"msg\": \"Invalid/incorrect password: @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\\r\\n@         WARNING: UNPROTECTED PRIVATE KEY FILE!          @\\r\\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\\r\\nPermissions 0777 for \'/media/mz/BA96E8A396E860FD/Project/Python/ihsan_ansible/static/user_key/20221227172057_private\' are too open.\\r\\nIt is required that your private key files are NOT accessible by others.\\r\\nThis private key will be ignored.\\r\\nLoad key \\\"/media/mz/BA96E8A396E860FD/Project/Python/ihsan_ansible/static/user_key/20221227172057_private\\\": bad permissions\\r\\nPermission denied, please try again.\", \"unreachable\": true}<br /><br />PLAY RECAP *********************************************************************<br />103.167.34.139             : ok=0    changed=0    unreachable=1    failed=0    skipped=0    rescued=0    ignored=0   <br /><br />', '2022-12-27 17:31:55', '2022-12-27 17:31:55', NULL);
INSERT INTO `history_action` VALUES (12, 'Test Connection', 4, '<br />PLAY [TestConnection] **********************************************************<br /><br />TASK [Gathering Facts] *********************************************************<br />ok: [103.167.34.139]<br /><br />TASK [Test Connection] *********************************************************<br />ok: [103.167.34.139]<br /><br />PLAY RECAP *********************************************************************<br />103.167.34.139             : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   <br /><br />', '2022-12-27 17:32:12', '2022-12-27 17:32:12', NULL);
INSERT INTO `history_action` VALUES (13, 'Change Port SSH', 4, '<br />PLAY [ChangePortPlayBook] ******************************************************<br /><br />TASK [Gathering Facts] *********************************************************<br />ok: [103.167.34.139]<br /><br />TASK [Chnage SSH PORT] *********************************************************<br />changed: [103.167.34.139]<br /><br />RUNNING HANDLER [restart ssh] **************************************************<br />changed: [103.167.34.139]<br /><br />PLAY RECAP *********************************************************************<br />103.167.34.139             : ok=3    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   <br /><br />', '2022-12-27 17:40:36', '2022-12-27 17:40:36', NULL);
INSERT INTO `history_action` VALUES (14, 'Remove User From Server', 1, '<br />PLAY [RemoveUserPlaybook] ******************************************************<br /><br />TASK [Remove User] *************************************************************<br />changed: [103.167.35.71]<br /><br />PLAY RECAP *********************************************************************<br />103.167.35.71              : ok=1    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   <br /><br />', '2022-12-27 17:43:04', '2022-12-27 17:43:04', NULL);
INSERT INTO `history_action` VALUES (15, 'Add user to Server', 1, '<br />PLAY [CreateNewUserPlaybook] ***************************************************<br /><br />TASK [Create User] *************************************************************<br />changed: [103.167.35.71]<br /><br />TASK [Deploy SSH Public Key] ***************************************************<br />changed: [103.167.35.71]<br /><br />PLAY RECAP *********************************************************************<br />103.167.35.71              : ok=2    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   <br /><br />', '2022-12-27 17:53:07', '2022-12-27 17:53:07', NULL);

-- ----------------------------
-- Table structure for server_user_credential
-- ----------------------------
DROP TABLE IF EXISTS `server_user_credential`;
CREATE TABLE `server_user_credential`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `private_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `public_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of server_user_credential
-- ----------------------------
INSERT INTO `server_user_credential` VALUES (1, 'Default', 'root', 'Skripsi123', '20221227144841_private', '20221227155729_public.pub', '2022-12-27 14:48:41', '2022-12-27 15:58:47', NULL);
INSERT INTO `server_user_credential` VALUES (2, 'Root 2', 'root', '123123', '20221227172057_private', '20221227172057_public.pub', '2022-12-27 17:20:58', '2022-12-27 17:20:58', NULL);

-- ----------------------------
-- Table structure for servers
-- ----------------------------
DROP TABLE IF EXISTS `servers`;
CREATE TABLE `servers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `user_credential_id` int(11) NULL DEFAULT NULL,
  `status` int(255) NULL DEFAULT 0,
  `port` int(255) NULL DEFAULT 22,
  `connection_test` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of servers
-- ----------------------------
INSERT INTO `servers` VALUES (1, '103.167.35.71', 1, 1, 22, 'Success', '2022-12-08 13:51:20', '2022-12-27 10:28:30', NULL);
INSERT INTO `servers` VALUES (2, '103.167.35.72', 1, 0, 22, 'Success', '2022-12-08 14:01:43', '2022-12-26 16:06:30', NULL);
INSERT INTO `servers` VALUES (4, '103.167.34.139', 1, 1, 22, 'Success', '2022-12-27 16:51:55', '2022-12-27 17:54:49', '2022-12-27 17:54:49');

-- ----------------------------
-- Table structure for sshuser
-- ----------------------------
DROP TABLE IF EXISTS `sshuser`;
CREATE TABLE `sshuser`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pub_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sshuser
-- ----------------------------
INSERT INTO `sshuser` VALUES (1, 'bambang', 'bambang.pub', '2022-12-19 11:32:57', '2022-12-19 16:34:31', NULL);

-- ----------------------------
-- Table structure for sshuser_server
-- ----------------------------
DROP TABLE IF EXISTS `sshuser_server`;
CREATE TABLE `sshuser_server`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_id` int(11) NULL DEFAULT NULL,
  `sshuser_id` int(11) NULL DEFAULT NULL,
  `is_sync` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sshuser_server
-- ----------------------------
INSERT INTO `sshuser_server` VALUES (1, 1, 1, 1, '2022-12-19 16:06:38', '2022-12-27 17:53:07', NULL);
INSERT INTO `sshuser_server` VALUES (2, 4, 1, 0, '2022-12-27 17:41:55', '2022-12-27 17:42:14', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', '$2b$12$p9p6RvrDfBbQD0T9cLcMyOK91iVqYWpGWjXntpPcmyFKUZXdJNuUO', 'Administrator', '2022-01-24 09:34:17', '2021-12-27 10:45:31', NULL);
INSERT INTO `users` VALUES (2, 'bambangkun', '$2b$12$owxie6OuCcpyW6taBAAX5uW/qmWg6eno3wGd6rVgdRqtfjnxmnqQ.', 'Bambang', '2022-12-06 15:34:06', '2022-12-23 17:27:05', NULL);
INSERT INTO `users` VALUES (3, 'setiawan', '$2b$12$xUIR7CHpRdQgZdHKt1mTMOf1/OJd0nGCjfOT3jtsnE2OfmFy3X9Uu', 'Setiawan', '2022-12-23 16:30:49', '2022-12-23 16:30:49', NULL);
INSERT INTO `users` VALUES (4, 'kamal', '$2b$12$ZC7NvWvUJSd/7QYu9jFWHu0gQZ0FTYG72jY3zZJdaVCNfYJ3IGct.', 'Kamal', '2022-12-27 11:03:41', '2022-12-27 11:03:45', '2022-12-27 11:03:45');

SET FOREIGN_KEY_CHECKS = 1;
